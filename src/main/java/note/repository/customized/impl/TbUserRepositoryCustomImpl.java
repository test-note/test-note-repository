/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.repository.customized.impl;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import note.repository.customized.TbUserRepositoryCustom;
import note.repository.entity.QTbUser;
import note.repository.entity.cuentas.UsuarioEntity;
import org.springframework.beans.factory.annotation.Autowired;

public class TbUserRepositoryCustomImpl implements TbUserRepositoryCustom {

    @Autowired
    private JPAQueryFactory factory;

    @Override
    public UsuarioEntity getUser(String email) throws Exception {
        QTbUser qTbUser = QTbUser.tbUser;
        BooleanBuilder builder = new BooleanBuilder();
        builder.and(qTbUser.email.eq(email));
        UsuarioEntity entity = factory.from(qTbUser)
                .select(Projections.bean(UsuarioEntity.class, qTbUser.id, qTbUser.email, qTbUser.password, qTbUser.rol))
                .where(builder).fetchOne();
        return entity;
    }

}
