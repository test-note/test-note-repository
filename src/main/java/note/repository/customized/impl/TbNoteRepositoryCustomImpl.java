/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.repository.customized.impl;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import note.repository.customized.TbNoteRepositoryCustom;
import note.repository.entity.QTbNote;
import note.repository.entity.TbNote;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author Martin Pilar
 */
public class TbNoteRepositoryCustomImpl implements TbNoteRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public PageImpl<TbNote> listNotes(String sortCampo, String sortOrden, String seach, PageRequest pageable) throws Exception {
        QTbNote note = QTbNote.tbNote;
        JPAQuery<QTbNote> jPAQuery = new JPAQuery<>(em);
        jPAQuery.select(note)
                .from(note);
        BooleanBuilder builder = new BooleanBuilder();
        builder.and(note.isDelete.isFalse());
        if (seach != null) {
            seach = seach.trim().toUpperCase();
            builder.and(note.note.upper().like("%" + seach + "%")
            );
        }
        if (sortCampo != null && sortOrden != null) {
            Path<Object> fieldPath = Expressions.path(Object.class, note, sortCampo);
            OrderSpecifier orderSpecifier = new OrderSpecifier(Order.valueOf(sortOrden.toUpperCase()), fieldPath);
            jPAQuery.orderBy(orderSpecifier);
        }
        jPAQuery.where(builder);
        jPAQuery.offset(pageable.getOffset());
        jPAQuery.limit(pageable.getPageSize());
        return new PageImpl(jPAQuery.fetch(), pageable, jPAQuery.fetchCount());
    }

}
