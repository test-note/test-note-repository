/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.repository.customized;

import note.repository.entity.cuentas.UsuarioEntity;

/**
 *
 * @author mpilar
 */
public interface TbUserRepositoryCustom {

    UsuarioEntity getUser(String email) throws Exception;

}
