/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.repository.customized;

import note.repository.entity.TbNote;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author Martin Pilar
 */
public interface TbNoteRepositoryCustom {

    PageImpl<TbNote> listNotes(String sortCampo, String sortOrden, String seach, PageRequest pageable) throws Exception;
}
