/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.repository;

import note.repository.customized.TbUserRepositoryCustom;
import note.repository.entity.TbUser;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mpilar
 */
@Repository
public interface TbUserRepository extends CrudRepository<TbUser, Integer>, QuerydslPredicateExecutor<TbUser>, TbUserRepositoryCustom {

}
