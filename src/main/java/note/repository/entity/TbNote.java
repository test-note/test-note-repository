/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.repository.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Martin Pilar
 */
@Entity
@Table(name = "TbNote")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbNote.findAll", query = "SELECT t FROM TbNote t"),
    @NamedQuery(name = "TbNote.findByNote", query = "SELECT t FROM TbNote t WHERE t.note = :note"),
    @NamedQuery(name = "TbNote.findByRegisterDate", query = "SELECT t FROM TbNote t WHERE t.registerDate = :registerDate"),
    @NamedQuery(name = "TbNote.findByUpdateDate", query = "SELECT t FROM TbNote t WHERE t.updateDate = :updateDate"),
    @NamedQuery(name = "TbNote.findById", query = "SELECT t FROM TbNote t WHERE t.id = :id"),
    @NamedQuery(name = "TbNote.findByIsDelete", query = "SELECT t FROM TbNote t WHERE t.isDelete = :isDelete")})
public class TbNote implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "note")
    private String note;
    @Basic(optional = false)
    @Column(name = "register_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registerDate;
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "is_delete")
    private boolean isDelete;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TbUser userId;

    public TbNote() {
    }

    public TbNote(Integer id) {
        this.id = id;
    }

    public TbNote(Integer id, String note, Date registerDate, boolean isDelete) {
        this.id = id;
        this.note = note;
        this.registerDate = registerDate;
        this.isDelete = isDelete;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public TbUser getUserId() {
        return userId;
    }

    public void setUserId(TbUser userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbNote)) {
            return false;
        }
        TbNote other = (TbNote) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "note.repository.entity.TbNote[ id=" + id + " ]";
    }
    
}
