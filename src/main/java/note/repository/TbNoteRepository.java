/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package note.repository;

import note.repository.customized.TbNoteRepositoryCustom;
import note.repository.entity.TbNote;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Martin Pilar
 */
@Repository
public interface TbNoteRepository extends CrudRepository<TbNote, Integer>, QuerydslPredicateExecutor<TbNote>, TbNoteRepositoryCustom {

}
